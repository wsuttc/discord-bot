const { Command, PrefixSupplier } = require("discord-akairo");
const { Message, MessageEmbed } = require("discord.js");
const { stripIndents } = require("common-tags");

module.exports = class HelpCommand extends Command {
  constructor() {
    super("help", {
      aliases: ["help"],
      description: {
        content:
          "Displays a list of available command, or detailed information for a specific command.",
        usage: "[command]",
      },
      category: "util",
      clientPermissions: ["EMBED_LINKS"],
      ratelimit: 2,
      args: [
        {
          id: "command",
          type: "commandAlias",
        },
      ],
    });
  }

  async exec(message, { command }) {
    const prefix = this.prefix;

    // If a specific command not provided, list all commands
    if (!command) {
      const embed = new MessageEmbed().setColor("AQUA").addField(
        "❯ Commands",
        stripIndents`A list of available commands.
                    For additional info on a command, type \`${prefix}help <command>\`
                `
      );

      // Scan each command category and list commands with their first alias
      for (const category of this.handler.categories.values()) {
        embed.addField(
          `❯ ${category.id.replace(/(\b\w)/gi, (lc) => lc.toUpperCase())}`,
          `${category
            .filter((cmd) => cmd.aliases.length > 0)
            .map((cmd) => `\`${cmd.aliases[0]}\``)
            .join(" ")}`
        );
      }

      return message.util.send(embed);
    }

    // A specific command was provided if we get to this point; show help for that command

    const embed = new MessageEmbed()
      .setColor([155, 200, 200])
      // Command's description.usage property
      .setTitle(
        `\`${command.aliases[0]} ${
          command.description.usage ? command.description.usage : ""
        }\``
      )
      // Command's description.content property
      .addField(
        "❯ Description",
        `${command.description.content ? command.description.content : ""} ${
          command.description.ownerOnly ? "\n**[Owner Only]**" : ""
        }`
      );

      // If there are more than one ways to use this command, list the additional aliases
    if (command.aliases.length > 1)
      embed.addField("❯ Aliases", `\`${command.aliases.join("` `")}\``, true);

      // If description.examples provided, list examples
    if (command.description.examples && command.description.examples.length)
      embed.addField(
        "❯ Examples",
        `\`${command.aliases[0]} ${command.description.examples.join(
          `\`\n\`${command.aliases[0]} `
        )}\``,
        true
      );

      // If description.notes exists, put it at the bottom (such as conditions for the command)
    if (command.description.notes)
      embed.addField("❯ Notes", command.description.notes);

    return message.util.send(embed);
  }
};
