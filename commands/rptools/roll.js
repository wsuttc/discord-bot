const { Command, Argument } = require("discord-akairo");
const { MessageEmbed } = require("discord.js");

// We have to use the umd module because Akairo is not written in ESM
const { DiceRoller, Parser } = require("rpg-dice-roller/lib/umd/bundle.js");

module.exports = class RollCommand extends Command {
  constructor() {
    super("roll", {
      aliases: ["roll", "dice"],
      description: {
        content: "Roll dice with standard dice notation. See https://greenimp.github.io/rpg-dice-roller/guide/notation/ .",
        usage: '[rolls:numberOfRoles] diceNotation',
        examples: ["2d20", "rolls:3 d6+3", "3d20d1+d6", "{4d6+2d8-3d30}d3 rolls:4"],
      },
      category: 'rptools',
      clientPermissions: ['EMBED_LINKS'],
      args: [
        {
          id: "rolls",
          type: Argument.range('integer', 1, 10, true),
          default: 1,
          prompt: {
            start: "How many times do you want me to roll?",
            retry:
              "I cannot roll less than 1 or more than 10 times. Please specify a new number of times to roll.",
            optional: true,
          },
          match: "option",
          flag: "rolls:"
        },
        {
          id: "notation",
          match: "rest",
          prompt: {
            start: "Please specify the dice notation for your roll. See https://greenimp.github.io/rpg-dice-roller/guide/notation/ .",
            retry:
              "Sorry, that was an invalid dice notation. Please try again. See https://greenimp.github.io/rpg-dice-roller/guide/notation/ .",
            time: 60000
          },
          type: Argument.validate('string', (m, p, str) => {
            try {
              str = str.replace(/\s+/g, '');
              let parsed = Parser.parse(str);
              if (parsed.constructor !== Array) return false; // Invalid notation
            } catch (e) {
              return false; // Also invalid notation
            }
            return true;
          }),
        },
      ],
    });
  }

  async exec(message, args) {
    // Construct the dice roller
    let roller = new DiceRoller();

    // Roll for however many times specified
    while (args.rolls > 0) {
      args.rolls--;
      roller.roll(args.notation.replace(/\s+/g, ''));
    }

    // Generate output
    let embed = new MessageEmbed()
      .setAuthor(`${message.author.tag}`, message.author.displayAvatarURL())
      .setColor("PURPLE")
      .setTitle(`Roll`)
      .setDescription("Here are your rolls:")
      .setFooter(`User ID: ${message.author.id}`);
    if (roller.log.length > 0) {
      roller.log.forEach((log, index) => {
        embed = embed.addField(`Roll #${index + 1}`, log.output);
      });
    }

    return message.reply(embed);
  }
};
