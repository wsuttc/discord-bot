# Changelog for Wright State Tabletop Club discord-bot

## 0.2.0-alpha

### Changed
- [BREAKING] config.token to config.discord.token

### Added
- CHANGELOG and README
- Inhibitors and listeners
- erlpack, bufferutil, and utf-8-validate packages

## 0.1.1-alpha - 2021-02-12

### Changed
- Added comments and example config file

## 0.1.0-alpha - 2021-02-11
Initial Commit

### Added
- Roll command
- Help command