const {
  AkairoClient,
  CommandHandler,
  InhibitorHandler,
  ListenerHandler,
} = require("discord-akairo");
const path = require("path");
const config = require("./config.json");

// Initiate client
class MyClient extends AkairoClient {
  constructor() {
    super(
      {
        ownerID: "794695911833993246", // Patrick Schmalstig
      },
      {
        disableMentions: "everyone", // We don't ever want to accidentally annoy users with an everyone mention
      }
    );

    // Construct handlers
    this.commandHandler = new CommandHandler(this, {
      directory: "./commands/",
      prefix: "ttc.",
      handleEdits: true,
      commandUtil: true,
      allowMention: true,
      argumentDefaults: {
        prompt: {
          timeout: ":x: Command timed out.",
          ended:
            ":x: You exceeded the allowed number of retries. Command was canceled.",
          cancel: ":x: Command was canceled.",
          retries: 3,
          time: 30000,
        },
      },
    });
    this.inhibitorHandler = new InhibitorHandler(this, {
      directory: "./inhibitors/",
    });
    this.listenerHandler = new ListenerHandler(this, {
      directory: "./listeners/",
    });

    // Load command handler
    this.commandHandler.useListenerHandler(this.listenerHandler);
    this.commandHandler.useInhibitorHandler(this.inhibitorHandler);
    this.commandHandler.loadAll();

    // Load inhibitor handler
    this.inhibitorHandler.loadAll();

    // Load listener handler
    this.listenerHandler.setEmitters({
      commandHandler: this.commandHandler,
      inhibitorHandler: this.inhibitorHandler,
      listenerHandler: this.listenerHandler,
    });
    this.listenerHandler.loadAll();
  }
}
const client = new MyClient();

// Login
client.login(config.discord.token);
