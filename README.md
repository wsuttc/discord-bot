# Wright State Tabletop Club discord-bot
This is a custom Discord Bot used in the Wright State Tabletop Club server. The bot is coded in the Akairo framework and is intended for use with the ttc-website package. It is NOT intended for public set-up, but the code may be used for inspiration in other projects.

**This bot is currently in ALPHA, meaning the API is very unstable, and many of the planned features are not yet implemented**.

## Features (Checked = complete, unchecked = planned, but not complete)
- [X] Roll command - roll dice according to standard dice notation
   - [ ] Roll Profiles - DMs can save certain notations under an easier-to-type profile name for use in the roll command
- [ ] Fantasy Name Generator integration / commands
- [ ] Campaign Management - Command to switch between campaigns, which affect the operations of campaign commands:
   - [ ] Add/edit/remove character sheets in a campaign; character sheet wizard
   - [ ] Commands for managing character sheet stats, such as for D&D stats and HP
   - [ ] Custom-coded homebrew character sheet management for homebrew campaigns (this will probably always require manual integration into the bot and in ttc-website).
   - [ ] Stats-based Rolls - DMs and players can use the roll command integrated with their campaign character sheets to roll stat checks without typing the stat modifier in.
- [ ] Discord Slash Commands support (still in beta; awaiting it to become stable and for Discord.js to implement it)