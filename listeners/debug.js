const { Listener } = require("discord-akairo");

class DebugListener extends Listener {
  constructor() {
    super("debug", {
      emitter: "client",
      event: "debug",
    });
  }

  async exec(debug) {
    console.debug(debug);
  }
}

module.exports = DebugListener;