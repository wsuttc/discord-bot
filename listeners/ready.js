const { Listener } = require("discord-akairo");

class ReadyListener extends Listener {
  constructor() {
    super("ready", {
      emitter: "client",
      event: "ready",
    });
  }

  async exec() {
    console.log(`Logged in as ${this.client.user.tag}! (${this.client.user.id})`);
    this.client.user.setActivity("prefix ttc.");
  }
}

module.exports = ReadyListener;
